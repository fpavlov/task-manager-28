package ru.t1.fpavlov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.api.service.IUserService;
import ru.t1.fpavlov.tm.command.AbstractCommand;
import ru.t1.fpavlov.tm.dto.Domain;
import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 30.01.2022.
 */
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    @Nullable
    public static final String ARGUMENT = null;

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIB};
    }

    @NotNull
    protected IUserService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    @NotNull
    protected ITaskService getTaskService() {
        return this.getServiceLocator().getTaskService();
    }

    @NotNull
    protected IProjectService getProjectService() {
        return this.getServiceLocator().getProjectService();
    }

    @NotNull
    protected Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(this.getProjectService().findAll());
        domain.setTasks(this.getTaskService().findAll());
        domain.setUsers(this.getUserService().findAll());
        return domain;
    }

    protected void setDomain(@NotNull final Domain domain) {
        if (domain == null) return;
        this.getProjectService().set(domain.getProjects());
        this.getTaskService().set(domain.getTasks());
        this.getUserService().set(domain.getUsers());
        this.getServiceLocator().getAuthService().logout();
    }
}

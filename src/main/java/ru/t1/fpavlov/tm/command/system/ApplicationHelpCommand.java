package ru.t1.fpavlov.tm.command.system;


import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.api.model.ICommand;
import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.util.Collection;

/**
 * Created by fpavlov on 07.12.2021.
 */
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Display available commands";

    @NotNull
    public static final String NAME = "help";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull  final Collection<AbstractCommand> commands = this.getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            System.out.println(command);
        }
    }

}

package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Update Task by index";

    @NotNull
    public static final String NAME = "task-update-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Task entity = this.findByIndex();
        @NotNull final String name = this.askEntityName();
        @NotNull final String description = this.askEntityDescription();
        @NotNull final String userId = this.getUserId();
        this.getTaskService().update(userId, entity, name, description);
    }

}

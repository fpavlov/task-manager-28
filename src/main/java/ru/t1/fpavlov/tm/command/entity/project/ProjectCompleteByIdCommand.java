package ru.t1.fpavlov.tm.command.entity.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Complete project by id";

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Project entity = this.findById();
        @NotNull final String userId = this.getUserId();
        this.getProjectService().changeStatus(userId, entity, Status.COMPLETED);
    }

}

package ru.t1.fpavlov.tm.command.system;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Display current application version";

    @NotNull
    public static final String NAME = "version";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println(this.getPropertyService().getApplicationVersion());
    }

}

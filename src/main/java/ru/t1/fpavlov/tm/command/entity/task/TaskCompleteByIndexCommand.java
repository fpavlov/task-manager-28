package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Complete Task by index";

    @NotNull
    public static final String NAME = "task-complete-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Task entity = this.findByIndex();
        @NotNull final String userId = this.getUserId();
        this.getTaskService().changeStatus(userId, entity, Status.COMPLETED);
    }

}

package ru.t1.fpavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.api.service.ICommandService;
import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.util.Collection;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    @NotNull
    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        this.commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argumentName) {
        if (argumentName == null || argumentName.isEmpty()) return null;
        return this.commandRepository.getCommandByArgument(argumentName);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String commandName) {
        if (commandName == null || commandName.isEmpty()) return null;
        return this.commandRepository.getCommandByName(commandName);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return this.commandRepository.getTerminalCommands();
    }

}

package ru.t1.fpavlov.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.service.IProjectTaskService;
import ru.t1.fpavlov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.fpavlov.tm.exception.entity.TaskNotFoundException;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.fpavlov.tm.model.Project;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/**
 * Created by fpavlov on 26.11.2021.
 */
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = this.projectRepository.findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final Task task = this.taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProject(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = this.taskRepository.findAllByProjectId(userId, project.getId());
        for (final Task item : tasks) this.taskRepository.remove(userId, item);
        this.projectRepository.remove(userId, project);
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) {
            throw new ProjectIdEmptyException();
        }

        @Nullable final Project project = this.projectRepository.findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = this.taskRepository.findAllByProjectId(userId, projectId);
        for (final Task item : tasks) this.taskRepository.remove(userId, item);
        this.projectRepository.remove(userId, project);
    }

    @Override
    public void unbindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = this.projectRepository.findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final Task task = this.taskRepository.findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}

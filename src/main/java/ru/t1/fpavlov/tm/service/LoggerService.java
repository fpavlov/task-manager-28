package ru.t1.fpavlov.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

/**
 * Created by fpavlov on 07.12.2021.
 */
public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = "./commands.xml";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = "./errors.xml";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = "./messages.xml";

    @NotNull
    private static final LogManager MANAGER = LogManager.getLogManager();

    @NotNull
    private static final Logger ROOT = Logger.getLogger("");

    @NotNull
    private static final Logger COMMANDS_LOGGER = getLoggerCommand();

    @NotNull
    private static final Logger ERRORS_LOGGER = getLoggerError();

    @NotNull
    private static final Logger MESSAGES_LOGGER = getLoggerMessage();

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    @NotNull
    public static Logger getLoggerCommand() {
        return Logger.getLogger(COMMANDS);
    }

    @NotNull
    public static Logger getLoggerError() {
        return Logger.getLogger(ERRORS);
    }

    @NotNull
    public static Logger getLoggerMessage() {
        return Logger.getLogger(MESSAGES);
    }

    {
        init();
        registry(COMMANDS_LOGGER, COMMANDS_FILE, false);
        registry(ERRORS_LOGGER, ERRORS_FILE, true);
        registry(MESSAGES_LOGGER, MESSAGES_FILE, true);
    }

    private void init() {
        try {
            MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            ROOT.severe(e.getMessage());
        }
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGES_LOGGER.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGES_LOGGER.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGES_LOGGER.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        ERRORS_LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

}

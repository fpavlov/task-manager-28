package ru.t1.fpavlov.tm.exception.field;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.exception.system.AbstractSystemException;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class NameEmptyException extends AbstractSystemException {

    @NotNull
    public NameEmptyException() {
        super("Error! Name is empty.");
    }

}

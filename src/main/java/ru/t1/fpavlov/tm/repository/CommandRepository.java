package ru.t1.fpavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;


/*
 * Created by fpavlov on 04.10.2021.
 */
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        @Nullable final String name = command.getName();
        if (name != null && !name.isEmpty()) this.mapByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) this.mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String commandArgument) {
        if (commandArgument == null || commandArgument.isEmpty()) return null;
        return this.mapByArgument.get(commandArgument);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String commandName) {
        if (commandName == null || commandName.isEmpty()) return null;
        return this.mapByName.get(commandName);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return this.mapByName.values();
    }

}

package ru.t1.fpavlov.tm.api.model;

import org.jetbrains.annotations.Nullable;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IHasName {

    @Nullable
    String getName();

    void setName(@Nullable final String name);

}

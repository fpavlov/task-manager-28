package ru.t1.fpavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 21.12.2021.
 */
public interface IRepository<M extends AbstractModel> {

    void clear();

    int getSize();

    @Nullable
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable final Comparator comparator);

    @NotNull
    List<M> findAll(@Nullable Sort sort);

    @NotNull
    Collection<M> add(@NotNull final Collection<M> items);

    @NotNull
    Collection<M> set(@NotNull final Collection<M> items);

    @Nullable
    M add(@Nullable final M item);

    @Nullable
    M findById(@Nullable final String id);

    @Nullable
    M findByIndex(@Nullable final Integer index);

    @Nullable
    M remove(@Nullable final M item);

    @Nullable
    M removeById(@Nullable final String id);

    @Nullable
    M removeByIndex(@Nullable final Integer index);

    boolean isIdExist(@Nullable final String id);

}

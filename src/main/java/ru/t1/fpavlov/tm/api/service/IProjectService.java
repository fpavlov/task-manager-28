package ru.t1.fpavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 10.10.2021.
 */
public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description);

    @NotNull
    Project changeStatus(
            @Nullable final String userId,
            @Nullable final Project project,
            @Nullable final Status status
    );

    @NotNull
    Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    @NotNull
    Project changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    );

    @NotNull
    Project update(
            @Nullable final String userId,
            @Nullable final Project project,
            @Nullable final String name,
            @Nullable final String description
    );

    @NotNull
    Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

    @NotNull
    Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    );

}

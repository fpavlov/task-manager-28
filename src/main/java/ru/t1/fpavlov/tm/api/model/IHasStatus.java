package ru.t1.fpavlov.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Status;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable final Status status);

}
